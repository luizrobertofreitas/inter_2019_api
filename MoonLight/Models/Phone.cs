namespace MoonLight.Models{
    public class Phone{
        public string Number { get; set; }
        public int PersonId { get; set; }        
    }
}