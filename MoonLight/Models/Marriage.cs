using System;
namespace MoonLight.Models
{
    public class Marriage
    {
        public Marriage()
        {
            this.Noiva = new Costumer();
            this.Noivo = new Costumer();
        }
        public int Id {get;set;}
        public int NoivoId {get;set;}
        public string NameNoivo { get; set; }
        public int NoivaId {get;set;}
        public string NameNoiva { get; set; }
        public string MainImage {get;set;}

        public TimeSpan BeginTime {get;set;}

        public TimeSpan FinalTime {get;set;}

        public DateTime Date {get;set;}

        public Costumer Noivo {get;set;}

        public Costumer Noiva {get;set;}
    }
}