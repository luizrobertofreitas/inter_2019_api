using System;

namespace MoonLight.Models
{
    public class Costumer 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Rg {get;set;}
        public string Cpf {get;set;}
        public DateTime BirthDay {get;set;}
        public string Number { get; set; }
    }
}