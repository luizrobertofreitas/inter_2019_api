namespace MoonLight.Models
{
    public class Address
    {
        public string Cep {get;set;}

        public int Number {get;set;}

        public string Complement {get;set;}

        public int PersonId {get;set;}
    }
}