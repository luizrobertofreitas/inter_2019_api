using System;

namespace MoonLight.Models{
    public class Guest{
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime DateInvite { get; set; }
    }
}