using System.Collections.Generic;

namespace MoonLight.Services
{
    public interface IService<T, TKey>
    {
        Result Create(T model);

        List<T> ListAll();

        T ReadOne(TKey id);

        Result Updade(T model);

        Result Delete(TKey id);
    }
}