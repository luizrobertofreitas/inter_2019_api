using System.Collections.Generic;
using MoonLight.Data;
using MoonLight.Models;

namespace MoonLight.Services
{
    public class CategoryService
    {
        public Result Create (Category model)
        {
            using(CategoryData categoryData = new CategoryData()){
                 var result = categoryData.Create(model); 
                return result;
            }
           
        }

        public Result Delete (int id)
        {
            using(CategoryData categoryData = new CategoryData()){
                var result = categoryData.Delete(id);
            return result;
            }
        }

        public Result Update(Category model)
        {
           using( CategoryData categoryData = new CategoryData()){
            var result = categoryData.Update(model);
            return result;
           }
        }

        public Category ReadOne(int id)
        {
             using(CategoryData categoryData = new CategoryData()){
                return categoryData.Read(id);
             }
             
        }

        public List<Category> Read()
        {
            using(CategoryData categoryData = new CategoryData()){
                return categoryData.Read();
            }
        }
    }
    
}