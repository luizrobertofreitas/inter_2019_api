using System.Collections.Generic;
using MoonLight.Data;
using MoonLight.Models;

namespace MoonLight.Service{
    public class PlanService{
        

        public Result Create (Plan p){
            using(PlanData planData = new PlanData())
            return planData.Create(p);
        }

        public List<Plan> ReadAll(){
            using(PlanData planData = new PlanData())
            return planData.Read();
        }

        public Plan ReadOne(int id){
            using(PlanData planData = new PlanData())
            return planData.Read(id);
        }

        public Result Updade( Plan p){
            using(PlanData planData = new PlanData())
            return planData.Update(p);
        }

        public Result Delete(int id){
            using(PlanData planData = new PlanData())
             return planData.Delete(id);
        }

    }
}