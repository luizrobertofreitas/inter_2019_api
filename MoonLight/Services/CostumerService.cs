using System;
using System.Collections.Generic;
using MoonLight.Data;
using MoonLight.Models;
using MoonLight.Utils;

namespace MoonLight.Services
{
    public class CostumerService : IService<Costumer, int>
    {
        public Result Create(Costumer model)
        {
            Result result = new Result();
            result.Action = "Create Costumer";
            try
            {
                
                if(!ValidateCpf.IsCpf(model.Cpf))
                    result.Problems.Add("Invalid CPF");

                using(CostumerData costumerData = new CostumerData()){
                    if(result.Success)
                    costumerData.Create(model);
                }

                return result;
            }
            catch(Exception ex)
            {
                
                result.Problems.Add($"Problems when create costumer {ex.Message}");

                return result;
            }            
        }

        public Result Delete(int id)
        {
            using(CostumerData costumerData = new CostumerData())
            return costumerData.Delete(id);
        }

        public List<Costumer> ListAll()
        {
            using(CostumerData costumerData = new CostumerData())
            return costumerData.Read();
        }

        public Costumer ReadOne(int id)
        {
            using(CostumerData costumerData = new CostumerData())
            return costumerData.Read(id);
        }

        public Result Updade(Costumer model)
        {
            Result result = new Result();
            result.Action = "Update Costumer";
            try
            {
                if(!ValidateCpf.IsCpf(model.Cpf))
                    result.Problems.Add("Invalid CPF");

                using(CostumerData costumerData = new CostumerData()){
                    if(result.Success)
                    costumerData.Update(model);
                }

                return result;
            }
            catch(Exception ex)
            {
                result.Problems.Add($"Problems when update costumer {ex.Message}");
                return result;
            } 
        }

        public Costumer Login(string email, string password)
        {
            try
            {
                using(CostumerData data = new CostumerData())
                return data.Login(email, password);                        
            }
            catch (System.Exception)
            {                
                throw;
            }
        }
    }
}