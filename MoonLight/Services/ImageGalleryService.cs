using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using MoonLight.Data;
using MoonLight.Models;
using MoonLight.Utils;

namespace MoonLight.Services{
    public class ImageGalleryService{
        public Byte[] GetImage(string owner, int id, string path){
            var imageManager = new ImageManager();
            var byteIMage = imageManager.Get(owner , id, path);
            return byteIMage;
        }           
    }
}