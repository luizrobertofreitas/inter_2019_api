using System.Collections.Generic;
using MoonLight.Data;
using MoonLight.Models;

namespace MoonLight.Services{
    public class GuestService{
        public List<Guest> ReadAll(){
            using(var guestData = new GuestData())
            return guestData.Read();
        }

        public Guest ReadOne(int id){
            using(var guestData = new GuestData())
            return guestData.Read(id);
        }

        public Result Create(Guest g){
            using(var guestData = new GuestData())
            return guestData.Create(g);
        }

        public Result Update (Guest g){
            using(var guestData = new GuestData())
            return guestData.Update(g);
        }

        public Result Delete (int id){
            using(var guestData = new GuestData())
            return guestData.Delete(id);
        }
    }
}