namespace MoonLight.Utils
{
    /// <summary>
	/// Release validation in CPF
	/// </summary>
	public static class ValidateCpf
	{
	    public static bool IsCpf(string cpf)
	    {
			int[] multipli1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
			int[] multipli2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
			string tempCpf;
			string digito;
			int sum;
			int resto;

			cpf = cpf.Trim();
			cpf = cpf.Replace(".", "").Replace("-", "");

			if (cpf.Length != 11)
			return false;

			tempCpf = cpf.Substring(0, 9);
			sum = 0;

			for(int i=0; i<9; i++)
				sum += int.Parse(tempCpf[i].ToString()) * multipli1[i];

			resto = sum % 11;
			if ( resto < 2 )
				resto = 0;
			else
			resto = 11 - resto;

			digito = resto.ToString();

			tempCpf = tempCpf + digito;

			sum = 0;
			for(int i=0; i<10; i++)
				sum += int.Parse(tempCpf[i].ToString()) * multipli2[i];

			resto = sum % 11;
			if (resto < 2)
			resto = 0;
			else
			resto = 11 - resto;

			digito = digito + resto.ToString();

			return cpf.EndsWith(digito);
	      }
	}
}
    