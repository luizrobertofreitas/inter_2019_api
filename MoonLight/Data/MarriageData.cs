using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data
{
    public class MarriageData : ConnectionData
    {
        public int Create(Marriage model)
        {
            try
            {
                int idCreated = 0;
                string query = "INSERT INTO persons VALUES (@name, @email, @password);";        
                query += "DECLARE @persongenerate INT;";
                query += "SET @persongenerate = @@IDENTITY;";  
                query += "INSERT INTO costumers VALUES (@@IDENTITY, @rg, @cpf, @birthday);";

                if (model.NoivoId == 0)
                    query += "INSERT INTO marriages VALUES (@persongenerate, @noiva_id, @begin_time, @final_time, @date, null);";
                else
                    query += "INSERT INTO marriages VALUES (@noivo_id, @persongenerate, @begin_time, @final_time, @date, null);";

                query += "SELECT SCOPE_IDENTITY()";

                using (var cmd = new SqlCommand(query, base.connection))
                {

                    if (model.NoivaId == 0)
                        cmd.Parameters.AddWithValue("@noivo_id", model.NoivoId);
                    else
                        cmd.Parameters.AddWithValue("@noiva_id", model.NoivaId);

                    cmd.Parameters.AddWithValue("@begin_time", model.BeginTime);
                    cmd.Parameters.AddWithValue("@final_time", model.FinalTime);
                    cmd.Parameters.AddWithValue("@date", model.Date);

                    if (model.NoivaId == 0)
                    {
                        cmd.Parameters.AddWithValue("@name", model.Noiva.Name);
                        cmd.Parameters.AddWithValue("@email", model.Noiva.Email);
                        cmd.Parameters.AddWithValue("@password", model.Noiva.Password);
                        cmd.Parameters.AddWithValue("@rg", model.Noiva.Rg);
                        cmd.Parameters.AddWithValue("@cpf", model.Noiva.Cpf);
                        cmd.Parameters.AddWithValue("@birthday", model.Noiva.BirthDay);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@name", model.Noivo.Name);
                        cmd.Parameters.AddWithValue("@email", model.Noivo.Email);
                        cmd.Parameters.AddWithValue("@password", model.Noivo.Password);
                        cmd.Parameters.AddWithValue("@rg", model.Noivo.Rg);
                        cmd.Parameters.AddWithValue("@cpf", model.Noivo.Cpf);
                        cmd.Parameters.AddWithValue("@birthday", model.Noivo.BirthDay);
                    }

                    idCreated = Convert.ToInt32(cmd.ExecuteScalar());
                }

                return idCreated;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(Marriage model)
        {
            try
            {
                string query = "EXEC update_marriage @id, @noivo_id, @noiva_id, @begin_time, @final_time";

                SqlCommand cmd = new SqlCommand { Connection = base.connection, CommandText = query };

                cmd.Parameters.AddWithValue("@id", model.Id);
                cmd.Parameters.AddWithValue("@noivo_id", model.NoivoId);
                cmd.Parameters.AddWithValue("@noiva_id", model.NoivaId);
                cmd.Parameters.AddWithValue("@begin_time", model.BeginTime);
                cmd.Parameters.AddWithValue("@final_time", model.FinalTime);

                cmd.ExecuteNonQuery();
                
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Result Delete(int id)
        {
            try
            {
                Result result = new Result();
                result.Action = "Delete Marriage";

                string query = "DELETE FROM marriages WHERE id = @id";

                SqlCommand cmd = new SqlCommand { Connection = base.connection, CommandText = query };

                cmd.Parameters.AddWithValue("@id", id);

                cmd.ExecuteNonQuery();
                

                return result;
            }
            catch (Exception ex)
            {
                Result result = new Result();
                result.Action = "Delete Marriage";
                result.Problems.Add($"Problems when delete marriage {ex.Message}");
                return result;
            }
        }

        public Marriage Read(int id)
        {
            try
            {
                string query = "SELECT * FROM vj_marriages WHERE id = @id";

                SqlCommand cmd = new SqlCommand { Connection = base.connection, CommandText = query };

                cmd.Parameters.AddWithValue("@id", id);

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    Marriage marriage = new Marriage
                    {
                        Id = (int)reader["Id"],
                        NoivaId = (int)reader["NoivaId"],
                        NoivoId = (int)reader["NoivoId"],                        
                        BeginTime = (TimeSpan)reader["BeginTime"],
                        FinalTime = (TimeSpan)reader["FinalTime"],
                        Date = (DateTime)reader["Date"]
                    };

                    marriage.MainImage = reader["MainImage"] == System.DBNull.Value ? null : (string)reader["MainImage"];                    

                    marriage.Noivo.Name = (string)reader["NameNoivo"];
                    marriage.Noivo.Email = (string)reader["EmailNoivo"];
                    marriage.Noivo.Id = (int)reader["NoivoId"];
                    marriage.Noivo.Cpf = (string)reader["CpfNoivo"];
                    marriage.Noivo.Rg = (string)reader["RgNoivo"];

                    marriage.Noiva.Name = (string)reader["NameNoiva"];
                    marriage.Noiva.Email = (string)reader["EmailNoiva"];
                    marriage.Noiva.Id = (int)reader["NoivaId"];
                    marriage.Noiva.Cpf = (string)reader["CpfNoiva"];
                    marriage.Noiva.Rg = (string)reader["RgNoiva"];

                    
                    return marriage;
                }

                
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Marriage> Read()
        {
            try
            {
                string query = "SELECT * FROM v_marriages";

                SqlCommand cmd = new SqlCommand { Connection = base.connection, CommandText = query };

                SqlDataReader reader = cmd.ExecuteReader();

                List<Marriage> marriages = new List<Marriage>();

                while (reader.Read())
                {
                    Marriage marriage = new Marriage
                    {
                        Id = reader.GetInt32(0),
                        NoivaId = reader.GetInt32(1),
                        NoivoId = reader.GetInt32(2),
                        MainImage = reader.GetString(11),
                        BeginTime = reader.GetTimeSpan(12),
                        FinalTime = reader.GetTimeSpan(13)
                    };

                    marriages.Add(marriage);
                }

                
                return marriages;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public Marriage ReadOneByCoostumer(int id)
        {
            try
            {
                string query = "SELECT * FROM v_marriages WHERE IdNoivo = @id OR IdNoiva = @id";
                SqlCommand cmd = new SqlCommand { Connection = base.connection, CommandText = query };
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    Marriage marriage = new Marriage
                    {
                        Id = (int)reader["IdMarriage"],
                        NoivaId = (int)reader["IdNoiva"],
                        NameNoivo = (string)reader["NameNoivo"],
                        NoivoId = (int)reader["IdNoivo"],
                        NameNoiva = (string)reader["NameNoiva"],
                        MainImage = (string)reader["ImageMarriage"],
                        BeginTime = (TimeSpan)reader["BeginTime"],
                        FinalTime = (TimeSpan)reader["FinalTime"]
                    };

                    
                    return marriage;
                }

                
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public void SaveImage(string base64, int id)
        {
            try
            {

                string query = "UPDATE marriages SET main_image = @image WHERE id = @id";

                SqlCommand cmd = new SqlCommand { Connection = base.connection, CommandText = query };

                cmd.Parameters.AddWithValue("@image", base64);
                cmd.Parameters.AddWithValue("@id", id);

                cmd.ExecuteNonQuery();
                
            }
            catch (System.Exception)
            {
                throw;
            }
        }
    }
}