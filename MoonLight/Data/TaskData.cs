using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data{
    public class TaskData : ConnectionData
    {
        public Result Create(Task model)
        {
            try
            {
                Result result = new Result();
                result.Action = "Create Task";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "INSERT INTO tasks VALUES (@description, @name)";
                cmd.Parameters.AddWithValue("@description", model.Description);
                cmd.Parameters.AddWithValue("@name", model.Name);
                int test = cmd.ExecuteNonQuery();
                
                
                return result;
            }
            catch (Exception ex)
            {
                
                Result result = new Result();
                result.Action = "Created Task";

                result.Problems.Add($"Problems to create a Task {ex.Message}");

                return result;                
            }
            
        }

        public Result Delete(int id)
        {
           try
           {
                Result result = new Result();
                result.Action = "Delete Task";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "DELETE FROM tasks WHERE id = @id";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                

                return result;
           }
           catch (Exception ex) 
           {
              
              Result result = new Result();
            result.Action = "Delete Task";

            result.Problems.Add($"Problems to delete address {ex.Message}");

            return result; 
           }
        }

        public List<Task> Read(){
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * from Tasks";
            SqlDataReader reader = cmd.ExecuteReader();
            List<Task> tasks = new List<Task>();
            while(reader.Read()){
                Task task = new Task();
                task.Id = (int)reader["id"];
                task.Description = (string)reader["description"];
                task.Name = (string)reader["name"];
                tasks.Add(task);
            }
            
            return tasks;
        }

        public Task Read(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM tasks WHERE id = @id";
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataReader reader = cmd.ExecuteReader();
            if(reader.Read()){
                Task task = new Task();
                task.Id = (int)reader["id"];
                task.Description = (string)reader["description"];
                task.Name = (string)reader["name"];
                
                return task;
            }
            
            return null;
        }

        public Result Update(Task model)
        {
            try
            {
                 Result result = new Result();
                result.Action = "Create Task";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "UPDATE tasks SET description = @description, name = @name WHERE id = @id";
                cmd.Parameters.AddWithValue("@description", model.Description);
                cmd.Parameters.AddWithValue("@name",model.Name);
                cmd.Parameters.AddWithValue("@id", model.Id);
                cmd.ExecuteNonQuery();
                
                
                return result;
            }
            catch (Exception ex)
            {
                Result result = new Result();
                result.Action = "Update Task";

                result.Problems.Add($"Problems to Update address {ex.Message}");

                return result; 
            }
        }
    }
}