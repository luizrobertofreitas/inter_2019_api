using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data
{
    public class MarriageGuestData : ConnectionData
    {
        public Result Create(MarriageGuest model)
        {
                var result = new Result();
                result.Action = "Created Marriage Guest";
            try
            {

                var cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC insert_marriage_guest @marriage_id, @guest_id, @status";
                cmd.Parameters.AddWithValue("@marriage_id", model.MarriageId);
                cmd.Parameters.AddWithValue("@guest_id", model.GuestId);
                cmd.Parameters.AddWithValue("@status", model.Status);                
                cmd.ExecuteNonQuery();
                
                return result;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public List<MarriageGuest> ListMarriageGuests(int marriageId)
        {
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM vj_marriages_guests WHERE marriageid = @marriageId";
            cmd.Parameters.AddWithValue("@marriageId", marriageId);
            var reader = cmd.ExecuteReader();
            var guestsMarriages = new List<MarriageGuest>();
            while(reader.Read()){
                var marriageGuest = new MarriageGuest();
                marriageGuest.MarriageId = (int)reader["MarriageId"];
                marriageGuest.GuestId = (int)reader["GuestId"];
                marriageGuest.GiftId = reader["GiftId"] == System.DBNull.Value ? 0 : (int)reader["GiftId"];
                marriageGuest.Status = (string)reader["Status"];                        
                marriageGuest.Guest.Name = (string)reader["GuestName"];
                marriageGuest.Guest.Email = (string)reader["GuestEmail"];
                guestsMarriages.Add(marriageGuest);
            }
            return guestsMarriages;
        }

        public Result Delete(int marriageId, int guestId)
        {
            var result = new Result();
            result.Action = "Delete Marriage Guest";
           try
           {
               var cmd = new SqlCommand();
               cmd.Connection = base.connection;
               cmd.CommandText = "DELETE FROM marriages_guests WHERE marriage_id = @marriage_id AND guest_id = @guest_id";
               cmd.Parameters.AddWithValue("@marriage_id", marriageId);
               cmd.Parameters.AddWithValue("@guest_id", guestId);
               cmd.ExecuteNonQuery();
               
               return result;
           }
           catch (System.Exception)
           {
                throw;         
           }
        }

        public int TotalAccepted(int marriageId)
        {
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT COUNT(*) Total FROM marriages_guests WHERE marriage_id = @marriage_id AND status = 'accepted'";
            cmd.Parameters.AddWithValue("@marriage_id", marriageId);            
            var reader = cmd.ExecuteReader();
            if(reader.Read()){
                return (int)reader["Total"];
            }

            return 0;
        }

        internal int TotalGuests(int marriageId)
        {
                        var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT COUNT(*) Total FROM marriages_guests WHERE marriage_id = @marriage_id";
            cmd.Parameters.AddWithValue("@marriage_id", marriageId);            
            var reader = cmd.ExecuteReader();
            if(reader.Read()){
                return reader["Total"] == System.DBNull.Value ? 0 : (int)reader["Total"];
            }
            
            return 0;
        }

        public List<MarriageGuest> Read()
        {
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM v_marriages_guests";
            var reader = cmd.ExecuteReader();
            var guestsMarriages = new List<MarriageGuest>();
            while(reader.Read()){
                var marriageGuest = new MarriageGuest();
                marriageGuest.MarriageId = (int)reader["MarriageId"];
                marriageGuest.GuestId = (int)reader["GuestId"];
                marriageGuest.GiftId = reader["GiftId"] == System.DBNull.Value ? 0 : (int)reader["GiftId"];
                marriageGuest.Status = reader["Status"] == System.DBNull.Value ? null : (string)reader["GiftId"];                             
                guestsMarriages.Add(marriageGuest);
            }
            return guestsMarriages;

        }

        public MarriageGuest Read(int marriageId, int guestId){
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM v_marriages_guests WHERE marriageid = @marriage_id AND guestid = @guest_id";
            cmd.Parameters.AddWithValue("@marriage_id", marriageId);
            cmd.Parameters.AddWithValue("@guest_id", guestId);
            var reader = cmd.ExecuteReader();
            if(reader.Read()){
                var marriageGuest = new MarriageGuest();
                marriageGuest.MarriageId = (int)reader["MarriageId"];
                marriageGuest.GuestId = (int)reader["GuestId"];                
                marriageGuest.GiftId = reader["GiftId"] == System.DBNull.Value ? 0 : (int)reader["GiftId"];
                marriageGuest.Status = reader["Status"] == System.DBNull.Value ? null : (string)reader["GiftId"];
                return marriageGuest;
            }

            return null;
        }

        public Result Update(MarriageGuest model)
        {
            var result = new Result();
            result.Action = "Update Marriage Guest";
            try
            {
                var cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC update_marriage_guest @marriage_id, @guest_id, @status";
                cmd.Parameters.AddWithValue("@marriage_id", model.MarriageId);
                cmd.Parameters.AddWithValue("@guest_id", model.GuestId);
                cmd.Parameters.AddWithValue("@status", model.Status);         
                cmd.ExecuteNonQuery();
                
                return result;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public void VerifyInvite(int marriageId, int guestId, bool accept)
        {
            var cmd = new SqlCommand();
            cmd.Connection = base.connection;
            
            string status = null;

            if(accept)
                status = "accepted";
            else
                status = "refused";

            Update(new MarriageGuest{MarriageId = marriageId, GuestId = guestId, Status = status});   

            
        }        


        public void InviteMarriageGuest(int marriageId, string name, string email, string password)
        {
            try
            {
                var cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC invite_marriage_guest @marriage_id, @name, @email, @password";
                cmd.Parameters.AddWithValue("@marriage_id", marriageId);
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@email", email);         
                cmd.Parameters.AddWithValue("@password", password);
                cmd.ExecuteNonQuery();
                
            }
            catch (System.Exception)
            {
                throw;
            }
        }
    }
}