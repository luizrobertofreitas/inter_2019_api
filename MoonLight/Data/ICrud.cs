namespace MoonLight.Data{
    public interface ICrud <G> where G : class{
        void Create(G model);
        G Read(int id);
        void Update(G model);
        void Delete(int id);
        
    }
}