using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data{
    public class PlanData : ConnectionData
    {
        public Result Create(Plan model)
        {
            try
            {
                Result result = new Result();
                result.Action = "Create Plan";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "INSERT INTO plans VALUES (@description, @period, @price)";
                cmd.Parameters.AddWithValue("@description", model.Description);
                cmd.Parameters.AddWithValue("@period", model.Period);
                cmd.Parameters.AddWithValue("@price", model.Price);
                int test = cmd.ExecuteNonQuery();
                
                
                return result;
            }
            catch (Exception ex)
            {
                
                Result result = new Result();
                result.Action = "Created Plan";
                result.Problems.Add($"Problems to created plan {ex.Message}");
                return result;                
            }
            
        }

        public Result Delete(int id)
        {
           try
           {
                Result result = new Result();
                result.Action = "Delete Plan";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "DELETE * FROM plans WHERE id = @id";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                

                return result;
           }
           catch (Exception ex) 
           {
              
              Result result = new Result();
            result.Action = "Delete Plan";

            result.Problems.Add($"Problems to delete address {ex.Message}");

            return result; 
           }
        }

        public List<Plan> Read(){
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * from plans";
            SqlDataReader reader = cmd.ExecuteReader();
            List<Plan> plans = new List<Plan>();
            while(reader.Read()){
                Plan plan = new Plan();
                plan.Id = (int)reader["id"];
                plan.Description = (string)reader["description"];
                plan.Period = (int)reader["period"];
                plan.Price = (decimal)reader["price"];
                plans.Add(plan);
            }
            
            return plans;
        }

        public Plan Read(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM plans WHERE id = @id";
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataReader reader = cmd.ExecuteReader();
            if(reader.Read()){
                Plan plan = new Plan();
                plan.Id = (int)reader["id"];
                plan.Description = (string)reader["description"];
                plan.Period = (int)reader["period"];
                plan.Price = (decimal)reader["price"];
                
                return plan;
            }
            
            return null;
        }

        public Result Update(Plan model)
        {
            try
            {
                 Result result = new Result();
                result.Action = "Create Plan";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "UPDATE plans SET description = @description, period = @period, price = @price WHERE id = @id";
                cmd.Parameters.AddWithValue("@description", model.Description);
                cmd.Parameters.AddWithValue("@period", model.Period);
                cmd.Parameters.AddWithValue("@price",model.Price);
                cmd.Parameters.AddWithValue("@id", model.Id);
                cmd.ExecuteNonQuery();
                
                
                return result;
            }
            catch (Exception ex)
            {
                Result result = new Result();
                result.Action = "Update Plan";

                result.Problems.Add($"Problems to Update address {ex.Message}");

                return result; 
            }
        }
    }
}