using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data {
    public class AddressData : ConnectionData
    {
        public Result Create(Address model)
        {
            try
            {
                Result result = new Result();
                result.Action = "Create Address";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC insert_address @cep, @number, @complement, @person_id";
                cmd.Parameters.AddWithValue("@cep", model.Cep);
                cmd.Parameters.AddWithValue("@number", model.Number);
                cmd.Parameters.AddWithValue("@complement", model.Complement);
                cmd.Parameters.AddWithValue("@person_id", model.PersonId);

                cmd.ExecuteNonQuery();

                return result;
            }
            catch(Exception ex)
            {                
                Result result = new Result();
                result.Action = "Create Address";
                result.Problems.Add($"Problems to create address {ex.Message}");
                return result;            
            }                        
        }

        public Result Delete(int person_id, string cep)
        {
            try
            {
                Result result = new Result();
                result.Action = "Delete Address";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "DELETE FROM addresses where person_id = @person_id AND cep = @cep";
                cmd.Parameters.AddWithValue("@person_id", person_id);
                cmd.Parameters.AddWithValue("@cep", cep);
                cmd.ExecuteNonQuery();
                return result;
            }
            catch(Exception ex)
            {                
                Result result = new Result();
                result.Action = "Delete Address";
                result.Problems.Add($"Problems to delete address {ex.Message}");
                return result;            
            }            
        }

        public Address Read(int person_id, string cep)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM addresses where person_id = @person_id AND cep = @cep";
            cmd.Parameters.AddWithValue("@person_id", person_id);
            cmd.Parameters.AddWithValue("@cep", cep);
            SqlDataReader reader =  cmd.ExecuteReader();
            if(reader.Read()){
                Address address = new Address();                
                address.Cep = reader.GetString(0);
                address.Number = reader.GetInt32(1);
                address.Complement = reader.GetString(2);
                address.PersonId = reader.GetInt32(3);
                return address;
            }
            return null;
        }

        public List<Address> Read()
        {   
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "select * from addresses";
            SqlDataReader reader =  cmd.ExecuteReader();
            List<Address> addresses = new List<Address>();

            while(reader.Read()){
                Address address = new Address();

                address.Cep = reader.GetString(0);
                address.Number = reader.GetInt32(1);
                address.Complement = reader.GetString(2);
                address.PersonId = reader.GetInt32(3);
                addresses.Add(address);
            }
            return addresses;
        }

        public Result Update(Address model, string oldCep)
        {
            try
            {
                Result result = new Result();
                result.Action = "Update Address";

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC update_address @cep, @old_cep, @number, @complement, @person_id";

                cmd.Parameters.AddWithValue("@cep", model.Cep);
                cmd.Parameters.AddWithValue("@old_cep", oldCep);
                cmd.Parameters.AddWithValue("@number", model.Number);
                cmd.Parameters.AddWithValue("@complement", model.Complement);
                cmd.Parameters.AddWithValue("@person_id", model.PersonId);

                cmd.ExecuteNonQuery();
                return result;
            }
            catch(Exception ex)
            {
                Result result = new Result();
                result.Action = "Update Address";

                result.Problems.Add($"Problems to update address {ex.Message}");

                return result;
            }            
        }
    }
}