using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using MoonLight.Models;

namespace MoonLight.Data
{
    public class CostumerData : ConnectionData
    {
        public void Create(Costumer model)
        {
            try
            {   
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC insert_costumer @name, @email, @password, @rg, @cpf, @birthDay, @number";
                cmd.Parameters.AddWithValue("@name", model.Name);
                cmd.Parameters.AddWithValue("@email", model.Email);
                cmd.Parameters.AddWithValue("@password", model.Password);
                cmd.Parameters.AddWithValue("@rg", model.Rg);
                cmd.Parameters.AddWithValue("@cpf", model.Cpf);
                cmd.Parameters.AddWithValue("@birthDay", model.BirthDay);
                cmd.Parameters.AddWithValue("@number", model.Number);
                cmd.ExecuteNonQuery();
                
            }
            catch(Exception)
            {                
                throw;          
            }                        
        }

        public Result Delete(int id)
        {
            try
            {
                Result result = new Result();
                result.Action = "Delete Costumer";
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC delete_custumer @id";
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                   
                return result;
            }
            catch(Exception ex)
            {                
                Result result = new Result();
                result.Action = "Delete Costumer";
                result.Problems.Add($"Problems to delete costumer {ex.Message}");
                return result;            
            }            
        }

        public Costumer Read(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM v_costumers where id = @id";
            cmd.Parameters.AddWithValue("@id", id);            
            SqlDataReader reader =  cmd.ExecuteReader();
            if(reader.Read()){
                Costumer costumer = new Costumer();                
                costumer.Id = (int)reader["Id"];
                costumer.Name = (string)reader["Name"];
                costumer.Email = (string)reader["Email"];
                costumer.Password = (string)reader["Password"];
                costumer.Rg = (string)reader["Rg"];
                costumer.Cpf = (string)reader["Cpf"];
                costumer.BirthDay = (DateTime)reader["Birthday"];
                
                return costumer;
            }
            
            return null;
        }

        public List<Costumer> Read()
        {   
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM v_costumers";
            SqlDataReader reader =  cmd.ExecuteReader();
            List<Costumer> costumers = new List<Costumer>();

            while(reader.Read()){
                Costumer costumer = new Costumer();                
                costumer.Id = (int)reader["Id"];
                costumer.Name = (string)reader["Name"];
                costumer.Email = (string)reader["Email"];
                costumer.Password = (string)reader["Password"];
                costumer.Rg = (string)reader["Rg"];
                costumer.Cpf = (string)reader["Cpf"];
                costumer.BirthDay = (DateTime)reader["Birthday"];
                costumers.Add(costumer);
            }
           
            
            return costumers;
        }

        public void Update(Costumer model)
        {
            try
            {   
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = base.connection;
                cmd.CommandText = "EXEC update_costumer @id, @name, @email, @password, @rg, @cpf, @birthday";
                cmd.Parameters.AddWithValue("@id", model.Id);
                cmd.Parameters.AddWithValue("@name", model.Name);
                cmd.Parameters.AddWithValue("@email", model.Email);
                cmd.Parameters.AddWithValue("@password", model.Password);
                cmd.Parameters.AddWithValue("@rg", model.Rg);
                cmd.Parameters.AddWithValue("@cpf", model.Cpf);
                cmd.Parameters.AddWithValue("@birthday", model.BirthDay);                
                cmd.ExecuteNonQuery();
                     
            }
            catch(Exception)
            {
                throw;
            }            
        }

        public Costumer Login(string email, string password)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = base.connection;
            cmd.CommandText = "SELECT * FROM v_costumers where email = @email AND password = @password";
            cmd.Parameters.AddWithValue("@email", email);            
            cmd.Parameters.AddWithValue("@password", password);
            SqlDataReader reader =  cmd.ExecuteReader();
            if(reader.Read()){
                Costumer costumer = new Costumer();                
                costumer.Id = (int)reader["Id"];
                costumer.Name = (string)reader["Name"];
                costumer.Email = (string)reader["Email"];
                costumer.Password = (string)reader["Password"];
                costumer.Rg = (string)reader["Rg"];
                costumer.Cpf = (string)reader["Cpf"];
                costumer.BirthDay = (DateTime)reader["Birthday"];
                
                return costumer;
            }

            
            
            return null;
        }
    }
}