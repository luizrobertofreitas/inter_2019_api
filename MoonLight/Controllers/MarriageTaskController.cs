using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers
{
    [Authorize]
    [EnableCors("AllowMyOrigin")]
    [Route("/api/[controller]")]
    [ApiController]
    public class MarriageTaskController : ControllerBase
    {
        private readonly MarriageTaskService service;
        public MarriageTaskController(MarriageTaskService _service)
        {
            service = _service;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public ActionResult<List<MarriageTask>> Get()
        {
            return service.ReadAll();
        }

        [HttpGet("{marriageId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<List<MarriageTask>> MarriageTask(int marriageId)
        {
            return service.ReadByMarriage(marriageId);
        }

        [HttpGet("MarriageTasks/{marriageId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<List<MarriageTask>> MarriageTasks(int marriageId)
        {
            var all = service.ReadAll().ToList();
            return all.Where(p => p.MarriageId == marriageId).ToList();
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Result> Post(MarriageTask model)
        {
            return service.Create(model);
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Result> Put(MarriageTask model)
        {
            return service.Update(model);
        }

        [HttpDelete("{marriageId}/{taskId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Result> Delete(int marriageId, int taskId)
        {
            return service.Delete(marriageId, taskId);
        }


        [HttpGet("CompleteTask/{marriageId}/{taskId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Result> CompleteTask(int marriageId, int taskId)
        {
            return service.CompleteTask(marriageId, taskId);
        }

        [HttpGet("ReportTasks/{marriageId}")]
        public ActionResult<object> ReportTask(int marriageId)
        {
            return service.ReportTasks(marriageId);
        }        
    }
}