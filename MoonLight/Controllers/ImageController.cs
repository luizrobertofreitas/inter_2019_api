using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers
{
    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private static ImageGalleryService imageGalleryService = new ImageGalleryService();

        [HttpGet("{owner}/{id}/{path}")]
        public IActionResult Get(string owner, int id, string path){
            try
            {
                var memory = imageGalleryService.GetImage(owner,id,path);
                return File(memory, "image/jpg");
            }
            catch (System.Exception)
            {
                
                return BadRequest();
            }
        }

    }
}