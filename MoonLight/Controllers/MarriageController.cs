using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Data;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers
{
    [Authorize]
    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class MarriageController : ControllerBase
    {    
        private static MarriageService marriageService = new MarriageService();
     
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public ActionResult<List<Marriage>> Get()
        {            
            return marriageService.ListAll();
        }
       
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Marriage> Get(int id)
        {
            return marriageService.ReadOne(id);
        }

        [HttpGet("costumer/{id}")]

        public ActionResult<Marriage> GetByCostumer(int id)
        {
            var resp = marriageService.ReadOneByCoostumer(id);
            if(resp != null){
               return Ok(resp); 
            }
            return BadRequest(resp);
        }
        
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<object> Post(Marriage model)
        {           
            return marriageService.Create(model);            
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Result> Put(Marriage model)
        {            
            return marriageService.Updade(model);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Result> Delete(int id)
        {
            return marriageService.Delete(id);
        }
        
        [HttpPost("UploadFile/{marriageId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Result> UploadFile(int marriageId, IFormFile file)
        {            
            return marriageService.SaveImage(file, marriageId);
        }
    }
}
