using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers
{
    [Authorize]
    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class MarriageProviderController : ControllerBase
    {
        private readonly MarriageProviderService service;
        public MarriageProviderController(MarriageProviderService _service)
        {
            service = _service;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Result> Post(MarriageProvider model)
        {
            
            var rs = service.Create(model);         
            if(rs.Success)
            {
                HttpContext.Response.StatusCode = 201;
                return rs;
            }

            return BadRequest(rs);
        }

        [HttpGet("ReportMarriageProvider/{marriageId}")]
        public ActionResult<object> ReportMarriageProvider(int marriageId)
        {
            return service.ReportMarriageProvider(marriageId);
        }

        [HttpGet("{providerId}")]
        public ActionResult<List<MarriageProvider>> Get(int providerId)
        {
            return service.GetByProvider(providerId);
        }
    }
}