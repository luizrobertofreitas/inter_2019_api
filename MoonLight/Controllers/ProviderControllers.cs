using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers{
    
    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProviderController : ControllerBase{
       
         private static ProviderService providerService = new ProviderService();

         [HttpGet]
         public ActionResult<List<Provider>> Read(){
            List<Provider> providers = providerService.ReadAll();
            if(providers == null){
                return BadRequest(providers);
            }
            return Ok(providers);
         }

         [HttpGet("{id}")]
         public ActionResult<Provider> Read(int id){
             Provider provider = providerService.ReadOne(id);
             if(provider == null){
                return BadRequest(provider);
             }
             return Ok(provider);
         }
         
         [HttpPost]
         public ActionResult<Result> Create(Provider p){
             Result result = providerService.Create(p);
             if(!result.Success){
                 return BadRequest(result);
             }
             return Ok(result);
         }

        [HttpPut]
         public ActionResult<Result> Update(Provider p){
             Result result = providerService.Update(p);
             if(!result.Success){
                 return BadRequest(result);
             }

             return Ok(result);
         }
        
        [HttpDelete("{id}")]
         public ActionResult<Result> Delete(int id){
             var result = providerService.Delete(id);
             if(!result.Success){
                 return BadRequest(result);
             }
            return result;
         }

         [HttpPost("{id}/image")]
         public ActionResult<Result> Image(int id,  IFormFile file){
             var result = providerService.SaveImage(id, file);
            return Ok(result);
         }

         [HttpGet("{provider_id}/image")]
         public ActionResult<List<Image>> GetAllImage(int provider_id){
             var imageList = providerService.GetAllImage(provider_id);
             if(imageList == null){
                 return BadRequest(imageList);
             }
             return Ok(imageList);
         }
        
        [HttpDelete("{provider_id}/image/{id}")]
        public ActionResult<Result> DeleteImage(int provider_id, int id){
            var result = providerService.DeleteImageProc(provider_id, id);
            if(!result.Success){
                return BadRequest(result);
            }      
            return Ok(result);
        }
    }

}