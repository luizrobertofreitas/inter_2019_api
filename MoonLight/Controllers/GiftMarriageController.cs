using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers
{
    [Authorize]
    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]    
    [ApiController]
    public class GiftMarriageController : ControllerBase    
    {
        private readonly GiftMarriageService service;
        public GiftMarriageController(GiftMarriageService _service)
        {
            service = _service;
        }

        [HttpGet("{marriageId}")]
        public ActionResult<List<GiftMarriage>> Get(int marriageId)
        {
            return service.GetAll(marriageId);
        }

        [HttpPost]
        public ActionResult<Result> Post(GiftMarriage model)
        {
            var result = service.Create(model);
            if(result.Success)
            {
                HttpContext.Response.StatusCode = 201;
                return result;
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpDelete("{marriageId}/{giftId}")]
        public ActionResult<Result> Delete(int marriageId, int giftId)
        {
            return service.Delete(marriageId, giftId);
        }
    }
}