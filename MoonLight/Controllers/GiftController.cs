using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Data;
using MoonLight.Models;
using MoonLight.Service;

namespace MoonLight.Controllers
{
    [Authorize]
    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]    
    [ApiController]
    public class GiftController : ControllerBase
    {    
        private static GiftService giftService = new GiftService();

        private readonly IHostingEnvironment _env;

        public GiftController(IHostingEnvironment env)
        {
            _env = env;
        }
     
        [HttpGet]
        public ActionResult<List<Gift>> Get()
        {            
            return Ok(giftService.ReadAll());
        }
       
        [HttpGet("{marriageId}")]
        public ActionResult<List<Gift>> Get(int marriageId)
        {
            return Ok(giftService.ReadByMarriage(marriageId));
        }
        
        [HttpPost]
        public ActionResult<int> Post(Gift model)
        {           
            var id = giftService.Create(model);
            if(id > 0)
            {
                HttpContext.Response.StatusCode = 201;
                return id;   
            }
            else
                return BadRequest();
        }

        [HttpPut]
        public ActionResult<Result> Put(Gift model)
        {
            
            var result = giftService.Updade(model);

            if(result.Success)
                return Ok(result);
            else
                return BadRequest(result);
        }

        [HttpDelete("{giftId}")]
        public ActionResult<Result> Delete(int giftId)
        {
            var result = giftService.Delete(giftId);
            if(result.Success)
                return NoContent();
            else
                return BadRequest(result);
        }

        [HttpPost("UploadFile/{marriageId:int}/{giftId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Result> UploadFile(int marriageId, int giftId, IFormFile file)
        {         
            var result = giftService
                         .SaveImage(file, _env.WebRootPath, "imgGift", marriageId.ToString(), giftId);

            return result;
        }          
    }
}
