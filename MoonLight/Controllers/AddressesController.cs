using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MoonLight.Data;
using MoonLight.Models;

namespace MoonLight.Controllers
{

    [Authorize(Roles = "Costumer, Provider")]
    [Route("api/[controller]")]
    [ApiController]
    public class AddressesController : ControllerBase
    {    
     
        [HttpGet]
        public ActionResult<List<Address>> Get()
        {
            AddressData data = new AddressData();

            return Ok(data.Read());
        }
       
        [HttpGet("{personId}/{cep}")]
        public ActionResult<Address> Get(int personId, string cep)
        {
            AddressData data = new AddressData();

            return Ok(data.Read(personId, cep));
        }
        
        [HttpPost]
        public ActionResult<Result> Post(Address model)
        {
            AddressData data = new AddressData();

            var result = data.Create(model);

            if(result.Success)
            {
                HttpContext.Response.StatusCode = 201;
                return result;   
            }
            else
                return BadRequest(result);
        }

        [HttpPut("{oldCep}")]
        public ActionResult<Result> Put(string oldCep, Address model)
        {
            AddressData data = new AddressData();
            var result = data.Update(model, oldCep); 

            if(result.Success)
                return Ok(result);
            else
                return BadRequest(result);
        }

        // DELETE api/values/5
        [HttpDelete("{personId}/{cep}")]
        public ActionResult<Result> Delete(int personId, string cep)
        {
            AddressData data = new AddressData();
            
            var result = data.Delete(personId, cep);

            if(result.Success)
                return NoContent();
            else
                return BadRequest(result);
        }
    }
}
