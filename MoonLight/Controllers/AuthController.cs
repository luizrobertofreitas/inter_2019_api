using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MoonLight.Models;
using MoonLight.Services;

namespace MoonLight.Controllers
{
    [EnableCors("AllowMyOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        LoginService service;
        public AuthController(LoginService _service)
        {
            service = _service;
        }

        [HttpPost]
        public ActionResult<Object> SignIn(Login login)
        {
            var resp = service.ValidateLogin(login.Email, login.Password);

            if(resp != null)
            {

                return Ok(resp);           
            }                                                 

            return Unauthorized();            
        }
    }
}