using System.Collections.Generic;

namespace MoonLight
{
    public class Result
    {
        public string Action {get;set;}

        public bool Success
        {
            get{return _Problems == null || Problems.Count == 0;}            
        }

        private List<string> _Problems = new List<string>();

        public List<string> Problems
        {
            get{return _Problems;}
        }
    }
}