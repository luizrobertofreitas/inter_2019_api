CREATE DATABASE interdb
GO

USE interdb
GO

-- persons(id*, name, email, password)
CREATE TABLE persons(
	id				INT				NOT NULL IDENTITY PRIMARY KEY,
	name			VARCHAR(100)    NOT NULL,
	email			VARCHAR(100)	NOT NULL,
	password		VARCHAR(MAX)	NOT NULL
)
GO

-- phones(number*, #person_id*)
CREATE TABLE phones(
	number			VARCHAR(15) NOT NULL,
	person_id		INT			NOT NULL REFERENCES persons(id),
	PRIMARY KEY(number, person_id)
)
GO


-- addresses(cep*, #person_id*, number, complement)
CREATE TABLE addresses(
	cep		VARCHAR(25)		NOT NULL,
	number	INT				NOT NULL,
	complement	VARCHAR(255),
	person_id	INT			NOT NULL REFERENCES persons(id),
	PRIMARY KEY(cep, person_id)
)
GO

-- plans(id*, description, period, price)
CREATE TABLE plans(
	id		INT NOT NULL PRIMARY KEY IDENTITY,
	description VARCHAR(50) NOT NULL,
	period		INT NOT NULL,
	price		MONEY NOT NULL
)
GO

-- categories(id*, name)
CREATE TABLE categories(
	id INT NOT NULL PRIMARY KEY IDENTITY,
	name	VARCHAR(50) NOT NULL
)
GO
-- providers(#person_id*, plan_id, fantasy_name, cnpj, description)
CREATE TABLE providers(
	person_id INT NOT NULL REFERENCES persons(id) PRIMARY KEY,
	plan_id	  INT NOT NULL REFERENCES plans(id),
	fantasy_name		VARCHAR(100) NOT NULL,
	cnpj				VARCHAR(20)	 NOT NULL,
	description			VARCHAR(MAX) NOT NULL,
	category_id			INT			 NOT NULL REFERENCES categories(id)
)
GO

CREATE TABLE images(
	id				INT				NOT	NULL PRIMARY KEY IDENTITY,
	provider_id		INT				NOT NUll REFERENCES providers(person_id),
	path			VARCHAR(max)	NOT NULL
)
GO

-- costumers(#person_id*, rg, cpf, Birthday)
CREATE TABLE costumers(
	person_id	INT NOT NULL REFERENCES persons(id) PRIMARY KEY,
	rg			VARCHAR(15)	DEFAULT NULL,
	cpf			VARCHAR(15)	NOT NULL UNIQUE,
	birthday	DATE NOT NULL
)
GO

-- guests(#person_id*, status)
CREATE TABLE guests(
	person_id	INT NOT NULL REFERENCES persons(id) PRIMARY KEY,	
	status 		int,
	check (status IN(1,0))	
)
GO

-- marriages(id*, #noivo_id, #noiva_id, image, begin_time, final_time)
CREATE TABLE marriages(
	id INT NOT NULL PRIMARY KEY IDENTITY,
	noivo_id	INT DEFAULT NULL REFERENCES costumers(person_id),
	noiva_id	INT DEFAULT NULL REFERENCES costumers(person_id),
	begin_time	TIME,
	final_time	TIME,
	date DATE NOT NULL,
	main_image varchar(max) DEFAULT NULL
)
GO

CREATE TABLE gallery (
	id				INT				NOT	NULL PRIMARY KEY IDENTITY,
	marriage_id INT NOT NULL REFERENCES marriages(id),
	path       VARCHAR(MAX)
)
GO


CREATE TABLE gifts(
	id	INT NOT NULL PRIMARY KEY IDENTITY,
	name		VARCHAR(50) NOT NULL,
	price		DECIMAL(10,2) NOT NULL,	
	name_file 	VARCHAR(MAX)
)
GO


CREATE TABLE marriages_guests(
	marriage_id	INT NOT NULL REFERENCES marriages(id),
	guest_id	INT NOT NULL REFERENCES guests(person_id),
	gift_id		INT DEFAULT NULL REFERENCES gifts(id),
	status		VARCHAR(10)	NOT NULL CHECK(status IN('accepted', 'refused', 'not yet'))
	PRIMARY KEY(marriage_id, guest_id)
)
GO


-- marriages_providers(#marriage_id*, #provider_id*, price, message, status, date_contract)
CREATE TABLE marriages_providers(
	marriage_id INT NOT NULL REFERENCES marriages(id),
	provider_id	INT NOT NULL REFERENCES providers(person_id),
	price		MONEY DEFAULT NULL,
	message		VARCHAR(MAX) NOT NULL,
	status		VARCHAR(10) NOT NULL CHECK(status IN('engaged', 'analyze', 'canceled'))
)
GO

-- tasks(id*, name, description)
CREATE TABLE tasks(
	id INT NOT NULL PRIMARY KEY IDENTITY,
	name	VARCHAR(100) NOT NULL,
	description	VARCHAR(MAX) NOT NULL
)
GO

-- marriages_tasks(#marriage_id*, #task_id*, priority)
CREATE TABLE marriages_tasks(
	marriage_id	INT NOT NULL REFERENCES marriages(id),
	task_id		INT NOT NULL REFERENCES tasks(id),
	priority	INT NOT NULl,
	status		VARCHAR(10)
	PRIMARY KEY(marriage_id, task_id)
	CHECK (status in ('doing','completed', 'to do'))
)
GO

CREATE TABLE marriages_gifts (
  marriage_id int NOT NULL references marriages(id),
  gift_id int NOT NULL references gifts(id),
  status int NOT NULL,
  PRIMARY KEY (marriage_id, gift_id),
  CHECK (status IN(1,2))
)
GO

CREATE PROC insert_costumer @name VARCHAR(100), @email VARCHAR(100), @password VARCHAR(MAX), @rg VARCHAR(15), @cpf VARCHAR(15), @birthday DATE, @number VARCHAR(15)
AS
BEGIN
	INSERT INTO persons VALUES(@name, @email, @password);
	DECLARE @id INT
	SET @id = @@IDENTITY
	INSERT INTO costumers VALUES(@id, @rg, @cpf, @birthday);
	INSERT INTO phones VALUES(@number,@id);
END
GO

CREATE PROC update_costumer @id INT, @name VARCHAR(100), @email VARCHAR(100), @password VARCHAR(MAX), @rg VARCHAR(15), @cpf VARCHAR(15), @birthday DATE
AS
BEGIN
	UPDATE persons SET  name = @name, email = @email, password = @password WHERE id = @id;
	UPDATE costumers SET  rg = @rg, cpf = @cpf, birthday = @birthday WHERE person_id = @id;
END
GO

CREATE PROC delete_costumer @id int
AS
BEGIN
	DELETE  FROM persons WHERE id = @id;
	DELETE  FROM costumers WHERE person_id = @id;
	DELETE  FROM phones WHERE person_id = @id;
END
GO


CREATE PROC insert_provider @name VARCHAR(100), @email VARCHAR(100), @password VARCHAR(MAX), @plan_id INT, @fantasy_name VARCHAR(100), @cnpj VARCHAR(20), @description VARCHAR(MAX), @number VARCHAR(15), @category_id INT
AS
BEGIN
	INSERT INTO persons VALUES(@name, @email, @password);
	DECLARE @id INT
	SET @id = @@IDENTITY
	INSERT INTO providers VALUES(@id, @plan_id, @fantasy_name, @cnpj, @description,@category_id);
	INSERT INTO phones VALUES(@number, @id);
END
GO

CREATE PROC update_provider @id INT, @name VARCHAR(100), @email VARCHAR(100), @password VARCHAR(MAX), @plan_id INT, @fantasy_name VARCHAR(100), @cnpj VARCHAR(20), @description VARCHAR(MAX),@category_id INT
AS
BEGIN
	UPDATE persons SET name =@name, email= @email, password = @password WHERE id = @id;
	UPDATE providers SET  plan_id = @plan_id, fantasy_name = @fantasy_name, cnpj =  @cnpj, description = @description, category_id = @category_id WHERE person_id = @id;
END
GO

CREATE PROC delete_provider @id int
AS
BEGIN
	DELETE  FROM persons WHERE id = @id;
	DELETE  FROM providers WHERE person_id = @id;
	DELETE  FROM phones WHERE person_id = @id;
END
GO

CREATE PROC insert_guest @name VARCHAR(100), @email VARCHAR(100), @password VARCHAR(MAX), @date_invite DATETIME, @status BIT, @number VARCHAR(15)
AS
BEGIN
	INSERT INTO persons VALUES(@name, @email, @password);
	DECLARE @id INT
	SET @id = @@IDENTITY
	INSERT INTO guests VALUES(@id, @date_invite,@status);
	INSERT INTO phones VALUES(@number,@id);
END
GO

CREATE PROC update_guest @id INT, @name VARCHAR(100), @email VARCHAR(100), @password VARCHAR(MAX), @date_invite DATETIME
AS
BEGIN
	UPDATE persons SET name = @name, email = @email, password = @password WHERE id = @id;
	UPDATE guests SET  date_invite = @date_invite WHERE person_id = @id;
END
GO

CREATE PROC delete_guest @id int
AS
BEGIN
	DELETE  FROM persons WHERE id = @id;
	DELETE  FROM guests WHERE person_id = @id;
	DELETE  FROM phones WHERE person_id = @id;
END
GO


-- VIEW OF PROVIDERS 
-- TO ADD COLUMN ONLY ADD ONE LINE AND PUT , IF ISN'T THE LAST LINE
CREATE VIEW v_providers
AS 
	SELECT 
	persons.id Id,
	persons.name Name,
	persons.email Email,
	persons.password Password,
	providers.fantasy_name Fantasy_Name,
	providers.cnpj Cnpj,
	providers.description Description,
	providers.plan_id Plan_Id,
	providers.category_id  Category
	FROM persons
	INNER JOIN providers ON persons.id = providers.person_id
	INNER JOIN categories ON providers.category_id = categories.id
GO

-- VIEW OF COSTUMERS 
-- TO ADD COLUMN ONLY ADD ONE LINE AND PUT , IF ISN'T THE LAST LINE
CREATE VIEW v_costumers
AS
	SELECT 
	persons.id Id,
	persons.name Name,
	persons.email Email,
	persons.password Password,
	costumers.rg Rg,
	costumers.cpf Cpf,
	costumers.birthday Birthday
	FROM persons, costumers
	WHERE costumers.person_id = persons.id;
GO
-- VIEW OF GUESTS 
-- TO ADD COLUMN ONLY ADD ONE LINE AND PUT , IF ISN'T THE LAST LINE
CREATE VIEW v_guests
AS
	SELECT 
	persons.id Id,
	persons.name Name,
	persons.email Email,
	persons.password Password,
	guests.date_invite DateInvite
	FROM persons, guests
	WHERE guests.person_id = persons.id;
GO

CREATE PROC insert_address(@cep VARCHAR(25), @number INT, @complement VARCHAR(MAX), @person_id INT)
AS
BEGIN 
	INSERT INTO addresses VALUES(@cep, @number, @complement, @person_id);
END
GO

CREATE PROC update_address(@cep VARCHAR(25), @old_cep VARCHAR(25), @number INT, @complement VARCHAR(MAX), @person_id INT)
AS
BEGIN 
	UPDATE addresses 	
	SET 
	cep = @cep, 
	number = @number, 
	complement = @complement, 
	person_id = @person_id 
	WHERE addresses.person_id = @person_id AND addresses.cep = @old_cep;
END
GO

CREATE VIEW v_marriages_guests
AS 
	SELECT 
		m.id MarriageId,
		gu.person_id GuestId,
		gi.id GiftId,
		marriages_guests.status Status,
		p.name GuestName,
		p.email GuestEmail,
		gi.price GiftPrice,
		gi.file_name GiftImage		
		FROM marriages_guests
		INNER JOIN marriages AS m ON m.id = marriages_guests.marriage_id
		INNER JOIN guests AS gu ON gu.person_id = marriages_guests.guest_id
		INNER JOIN persons AS p ON p.id = gu.person_id
		LEFT JOIN gifts AS gi ON gi.id = marriages_guests.gift_id
GO

CREATE VIEW v_marriages
AS 		
SELECT 
	marriages.id IdMarriage,
	pNoiva.id IdNoiva,
	pNoivo.id IdNoivo,
	pNoivo.name NameNoivo,
	pNoivo.email EmailNoivo,
	pNoiva.name NameNoiva,
	pNoiva.email EmailNoiva,
	noivo.cpf CpfNoivo,
	noiva.cpf CpfNoiva,
	noivo.rg RgNoivo,
	noiva.rg RgNoiva,
	marriages.main_image ImageMarriage,
	marriages.begin_time BeginTime,
	marriages.final_time FinalTime
FROM marriages
LEFT JOIN costumers AS noivo ON marriages.noivo_id = noivo.person_id
LEFT JOIN costumers AS noiva ON marriages.noiva_id = noiva.person_id
INNER JOIN persons pNoivo ON noivo.person_id = pNoivo.id
INNER JOIN persons pNoiva ON noiva.person_id = pNoiva.id
GO

CREATE PROC insert_marriage_task @marriage_id int, @task_id int, @priority int, @status varchar(10)
AS
BEGIN
	INSERT INTO marriages_tasks VALUES (@marriage_id, @task_id, @priority, @status)
END
GO

CREATE VIEW vj_marriages_tasks
AS
	SELECT 
	tasks.id TaskId,
	marriages_tasks.marriage_id MarriageId,
	tasks.name Name,
	tasks.description Description,
	marriages_tasks.status Status,
	marriages_tasks.priority Priority	
	FROM marriages_tasks	
	INNER JOIN tasks ON tasks.id = marriages_tasks.task_id;
GO

CREATE VIEW vj_marriages_guests
AS
	SELECT 
		mg.marriage_id MarriageId,
		mg.guest_id GuestId,
		mg.gift_id GiftId,
		mg.status Status,
		p.name GuestName,
		p.email GuestEmail
		FROM marriages_guests mg
		INNER JOIN guests g ON mg.guest_id = g.person_id
		INNER JOIN persons p ON g.person_id = p.id
GO

CREATE VIEW vj_marriages_gifts
AS
	SELECT 
	mg.gift_id GiftId,
	mg.marriage_id MarriageId,
	mg.status Status,
	g.name Name,
	g.price Price,
	g.name_file NameFile 
	FROM marriages_gifts mg
	INNER JOIN gifts g ON g.id = mg.gift_id;
GO

CREATE VIEW vj_marriages
AS 
	SELECT
		ma.id Id,
		ma.noiva_id NoivaId,
		ma.noivo_id NoivoId,
		ma.main_image MainImage,
		ma.begin_time BeginTime,
		ma.final_time FinalTime,
		ma.date Date,
		pnoivo.name NameNoivo,
		pnoivo.email EmailNoivo,
		gnoivo.cpf CpfNoivo,
		gnoivo.rg RgNoivo,
		pnoiva.name NameNoiva,
		pnoiva.email EmailNoiva,
		gnoiva.cpf CpfNoiva,
		gnoiva.rg RgNoiva		
		FROM marriages ma
		INNER JOIN costumers gnoivo ON gnoivo.person_id = ma.noivo_id
		INNER JOIN persons pnoivo ON pnoivo.id = gnoivo.person_id
		INNER JOIN costumers gnoiva ON gnoiva.person_id = ma.noiva_id
		INNER JOIN persons pnoiva ON pnoiva.id = gnoiva.person_id
GO

CREATE VIEW report_tasks
AS
SELECT*
FROM
(
	SELECT 
	mt.marriage_id marriageId,	
	mt.status status,
	COUNT(*) Tasks,
	(
		SELECT 			
			COUNT(*)
			FROM 
			marriages_tasks
			WHERE marriage_id = mt.marriage_id
	) Total
	FROM marriages_tasks mt
	GROUP BY mt.marriage_id, mt.status) Dados
	WHERE Dados.status = 'completed';
GO


CREATE PROC insert_gift @name varchar(50), @price decimal(10,2)
AS 
BEGIN
	INSERT INTO gifts VALUES (@price, @name, null)
END
GO

  CREATE PROC update_gift @id int, @marriage_id int, @name varchar(10), @price decimal(10,2), @status varchar(10) 
AS 
BEGIN
	UPDATE gifts SET name = @name, price = @price WHERE id = @id
END
GO


CREATE PROC invite_marriage_guest @marriageId int, @name varchar(100), @email varchar(100), @password varchar(max)
AS 
BEGIN 
	INSERT INTO persons VALUES (@name, @email, @password);
	declare @idGenerate INT;
	set @idGenerate = @@IDENTITY
	INSERT INTO guests VALUES (@idGenerate, 1);
	INSERT INTO marriages_guests VALUES (@marriageId, @idGenerate, null, 'not yet');
END
GO

CREATE PROC insert_marriage @noivo_id int, @noiva_id int, @begin_time time, @final_time TIME, @data DATE
AS
BEGIN
  INSERT INTO marriages VALUES (@noivo_id, @noiva_id, @begin_time, @final_time, @data, null)
END
GO

CREATE PROC insert_gift_marriage @marriage_id int, @gift_id int, @status INT
AS 
BEGIN
	INSERT INTO marriages_gifts VALUES (@marriage_id, @gift_id, @status)
END
GO

CREATE VIEW v_marriages_tasks
AS 
	SELECT
		mt.marriage_id MarriageId,
		mt.task_id TaskId,
		mt.priority Priority,
		mt.status Status
		FROM marriages_tasks mt
GO

CREATE PROC insert_marriage_provider @marriage_id int, @provider_id int, @message varchar(max), @status varchar(10)
AS 
BEGIN
	INSERT INTO marriages_providers VALUES (@marriage_id, @provider_id, null, @message, @status);
END
GO

CREATE VIEW vj_marriages_providers
AS
SELECT 
 mp.provider_id ProviderId,
 m.date Date,
 mp.message Message,
 p.email Email,
 m.main_image MainImage 
FROM 
marriages_providers mp
INNER JOIN marriages m ON m.id = mp.marriage_id
INNER JOIN persons p ON p.id = m.noivo_id
GO